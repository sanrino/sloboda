var gulp           = require('gulp'),
		gutil          = require('gulp-util' ),
		sass           = require('gulp-sass'),
		browserSync    = require('browser-sync'),
		concat         = require('gulp-concat'),
		uglify         = require('gulp-uglify'),
		cleanCSS       = require('gulp-clean-css'),
		rename         = require('gulp-rename'),
		del            = require('del'),
		imagemin       = require('gulp-imagemin'),
		cache          = require('gulp-cache'),
		autoprefixer   = require('gulp-autoprefixer'),
		ftp            = require('vinyl-ftp'),
		notify         = require("gulp-notify"),
		fileinclude    = require('gulp-file-include');

// Normal
gulp.task('normal-js', function() {
	return gulp.src([
		'app/blocks/pageMain/pageMain.js',
		'app/js/common.js'
	])
	.pipe(concat('common.js'))
	.pipe(gulp.dest('app/_normal/js'))
	.pipe(browserSync.reload({stream: true}));
});

gulp.task('normal-libs-js', function() {
	return gulp.src([
		'app/libs/jquery/dist/jquery-3.3.1.min.js',
		// 'app/libs/bootstrap/dist/popper.min.js',
		// 'app/libs/bootstrap/dist/bootstrap.js',
		// 'app/libs/slick/dist/slick.min.js',
		])
	.pipe(concat('scripts.min.js'))
	// .pipe(uglify()) // Минимизировать весь js (на выбор)
	.pipe(gulp.dest('app/_normal/js'))
	.pipe(browserSync.reload({stream: true}));
});

gulp.task('normal-browser-sync', function() {
	browserSync({
		server: {
			baseDir: 'app/_normal'//Обновляет стран браузера когда компилир html
		},
		notify: false,
		// tunnel: true,
		// tunnel: "projectmane", //Demonstration page: http://projectmane.localtunnel.me
	});
});

gulp.task('normal-libs-min-css', function() {
	return gulp.src('app/sass/all.sass')
	.pipe(sass({outputStyle: 'expand'}).on("error", notify.onError()))
	.pipe(rename({suffix: '.min', prefix : ''}))
	.pipe(autoprefixer(['last 15 versions']))
	//.pipe(cleanCSS()) // Опционально, закомментировать при отладке
	.pipe(gulp.dest('app/_normal/css'))
	.pipe(browserSync.reload({stream: true}));
});

gulp.task('normal-css', function() {
	return gulp.src(['app/sass/fonts.sass','app/sass/main.sass'])
	.pipe(sass({outputStyle: 'expand'}).on("error", notify.onError()))
	.pipe(autoprefixer(['last 15 versions']))
	.pipe(gulp.dest('app/_normal/css'))
	.pipe(browserSync.reload({stream: true}));
});

gulp.task('normal-fileinclude', function() {
  gulp.src(['app/html/*.html'])
    .pipe(fileinclude({
      prefix: '@@',
      basepath: 'app/html'
    }))
    .pipe(gulp.dest('app/_normal'));
});

gulp.task('normal-img-min', function() {
	return gulp.src(['app/img/**/*','app/img/**/**/*'])
	.pipe(cache(imagemin()))
	.pipe(gulp.dest('app/_normal/img')); 
});

gulp.task('normal-fonts', function() {
	return gulp.src(['app/fonts/**/*','app/fonts/**/**/*'])
	.pipe(gulp.dest('app/_normal/fonts')); 
});

gulp.task('normal-watch', ['normal-fileinclude','normal-libs-min-css','normal-css','normal-js','normal-libs-js','normal-browser-sync'], function() {
	gulp.watch('app/sass/all.sass', ['normal-libs-min-css']);
	gulp.watch('app/blocks/**/*.sass', ['normal-css']);
	gulp.watch(['app/sass/fonts.sass','app/sass/main.sass'], ['normal-css']);
	gulp.watch('app/libs/**/*.js', ['normal-libs-js']);

	gulp.watch('app/blocks/**/*.js', ['normal-js']);
	
	gulp.watch('app/js/common.js', ['normal-js']);
	gulp.watch('app/blocks/**/*.html', ['normal-fileinclude']);
	gulp.watch('app/html/*.html', ['normal-fileinclude']);
	gulp.watch('app/_normal/*.html', browserSync.reload);
});

gulp.task('normal', ['normal-watch','normal-img-min','normal-fonts']);
